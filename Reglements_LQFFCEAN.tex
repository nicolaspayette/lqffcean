\documentclass[10pt]{article}

\usepackage{microtype}

\usepackage{multicol}
\setlength{\columnsep}{20pt}
\setlength{\columnseprule}{0.1pt}

\usepackage{fontspec}
\setmainfont[Ligatures=TeX]{Linux Libertine O}
\setmonofont[Scale=MatchLowercase]{DejaVu Sans Mono}

\usepackage{titlesec}
\titlespacing{\section}{0cm}{1em}{1em}

\usepackage{enumitem}
\setlist[enumerate, 1]{label=\thesection.\arabic*}
\setlist[enumerate, 2]{label*=.\arabic*}
\setlist[enumerate, 3]{label*=.\arabic*}

\usepackage{polyglossia}
\setmainlanguage{french}
\usepackage[french=guillemets*]{csquotes}
\MakeOuterQuote{"}
\usepackage{numprint}

\usepackage[hidelinks]{hyperref}

\usepackage[margin=2.25cm]{geometry}

\usepackage[all]{nowidow}

\newcommand{\kw}[1]{\textsc{#1}}

\begin{document}

\title{
  Règlements L.Q.F.F.C.E.A.N. \\
  \vspace{0.5em} \small \textit{Ligue Québécoise, Française, Francophone,
  Canadienne de l’Est de l’Amérique du Nord}
} \maketitle

\begin{multicols}{2}

\section{Rôles}
\begin{enumerate}

  \item Chaque \kw{club} de la LQFFCEAN peut être la propriété d'une ou
  plusieurs personnes, mais ne constitue qu'une seule entité aux yeux de la
  LQFFCEAN.

  \begin{enumerate}

    \item Chaque \kw{club} est représenté par une et une seule personne: son
    \kw{directeur général} (ci-après, \kw{DG}).

    \item C'est le \kw{DG} qui, en toutes circonstances, agit au nom du \kw{club}
    et prend les décisions le concernant.

  \end{enumerate}

  \item Le \kw{commissaire} est l'officier administratif principal de la
  LQFFCEAN.

  \begin{enumerate}

    \item Le \kw{commissaire} est investi de toute l'autorité nécessaire pour
    assurer le bon fonctionnement de la LQFFCEAN.

    \item Le \kw{commissaire} est élu lors de la \kw{rencontre de pré-saison}.

  \end{enumerate}

  \item Le \kw{législateur} est l'officier administratif responsable de la
  rédaction, de la maintance et de l'interprétation des règlements de la
  LQFFCEAN.

  \begin{enumerate}

    \item L'autorité du \kw{législateur} est subordonnée à celle du
    \kw{commissaire}, sauf en ce qui a trait à la formulation des règlements, dont
    le \kw{législateur} est le seul responsable.

    \item Le \kw{législateur} est élu lors de la \kw{rencontre de pré-saison}.

  \end{enumerate}

\end{enumerate}

\section{Rencontres}

\begin{enumerate}

  \item Les DG se rencontrent quatre fois par saison:

  \begin{enumerate}

    \item La \kw{rencontre de pré-saison} a lieu en septembre et
    est consacrée à la mise à jour des règlements pour la saison à venir.

    \item La \kw{rencontre de repêchage initial} a généralement lieu lors de la
    première partie du Canadien.

    \item La \kw{rencontre de repêchage intermédiaire} a lieu
    entre la dernière de semaine de novembre et la dernière semaine de
    décembre.

    \item La \kw{rencontre de repêchage final} a lieu juste avant
    la date limite des échanges de la LNH.

  \end{enumerate}

  \item Un \kw{DG} qui ne peut être \kw{présent physiquement} à une
  \kw{rencontre} peut toutefois être \kw{présent virtuellement} s'il peut
  facilement être joint par le \kw{commissaire} à tout moment pendant la
  \kw{rencontre} pour exercer ses pouvoirs de \kw{DG}.

  \item Un \kw{DG} qui ne peut être \kw{présent}, ni \kw{physiquement}, ni
  \kw{virtuellement} à une \kw{rencontre} peut désigner un \kw{représentant} qui
  agira en son nom lors la \kw{rencontre}.

  \begin{enumerate}

    \item Le \kw{DG} absent doit communiquer avec le \kw{commissaire} pour lui
    faire connaître le \kw{représentant} désigné et l'étendue des pouvoirs
    délégués à ce dernier.

  \end{enumerate}

  \item \kw{Quorum}: il est nécessaire que $50\% + 1$ des \kw{DG} soient
  \kw{présents} à une rencontre pour que celle-ci puisse avoir lieu.

  \begin{enumerate}

    \item Un \kw{DG} ayant délégué un \kw{représentant} ou étant \kw{présent
    virtuellement} est considéré comme \kw{présent} pour les besoins du
    \kw{quorum}.

  \end{enumerate}

\end{enumerate}

\section{Processus démocratique}

\begin{enumerate}

  \item Ultimement, tous les aspects du fonctionnement de la LQFFCEAN reposent
  sur le vote démocratique. Il est \emph{toujours} possible de voter une
  modification ou une dérogation aux règlements.

  \item Tous les \kw{DG} peuvent proposer un vote, mais c'est la responsabilité
  du \kw{législateur} de formuler exactement les options proposées.

  \item Si le vote a lieu \emph{lors} d'une \kw{rencontre}:

  \begin{enumerate}

    \item L'option gagnante doit obtenir le vote de $50\% + 1$ des \kw{DG}
    \kw{présents} lors de la \kw{rencontre}.

    \item Le vote se prend normalement à main levée, mais un \kw{DG} peut
    exceptionnellement demander un vote secret. Si un vote secret est
    demandé, il en revient au \kw{commissaire} de déterminer le fonctionnement
    exact du vote.

  \end{enumerate}

  \item Si le vote a lieu \emph{à l'extérieur} d'une \kw{rencontre}:

  \begin{enumerate}

    \item L'option gagnante doit obtenir le vote de $50\% + 1$ de \emph{tous}
    les \kw{DG}.

    \item La tenue du vote est annoncée par courriel à tous les \kw{DG}.

    \item Chaque \kw{DG} a la responsabilité de consulter régulièrement ses
    courriels pour se tenir au courant de la tenue des votes.

    \item Chaque \kw{DG} doit communiquer son vote au \kw{commissaire}, par le
    moyen de son choix, le plus rapidement possible.

    \item Chaque \kw{DG} a le \emph{droit}, mais pas \emph{l'obligation}, de
    faire connaître son vote publiquement.

    \item Le vote se conclut dès qu'une option atteint le seuil de $50\% + 1$ de
    tous les \kw{DG}.

  \end{enumerate}

\end{enumerate}

\section{Ballottages}

\begin{enumerate}

  \item Il y a trois types de \kw{ballottages}:

  \begin{enumerate}

    \item Un \kw{ballottage régulier} permet à un \kw{DG} de remplacer n'importe
    quel joueur de son \kw{club} par un joueur \kw{disponible}
    (voir~\ref{itm:disponible}).

    \item Un \kw{ballottage de libération} permet à un \kw{DG} de remplacer un
    joueur \kw{libéré} (voir~\ref{itm:liberation}) par un joueur
    \kw{disponible}.

    \item Un \kw{ballottage d'expansion} permet à un \kw{DG} de remplacer un
    joueur perdu lors d'un \kw{repêchage d'expansion} par un joueur
    \kw{disponible} (voir section~\ref{sec:expansion}).

  \end{enumerate}

  \item \label{itm:nombre-ballottages} Chaque \kw{club} reçoit \textbf{trois}
  \kw{ballottages réguliers} par saison: un \kw{ballottage} de première ronde,
  un \kw{ballottage} de deuxième ronde et un \kw{ballottage} de troisième
  ronde.

  \item \label{itm:rondes-ballottages} Lors de la \kw{rencontre de repêchage
  initial}, on fait trois rondes de \kw{ballottages réguliers}, suivies d'autant de rondes de \kw{ballottages de
  libération} qu'il est nécessaire, suivies d'autant de rondes de
  \kw{ballottages d'expansion} qu'il est nécessaire.

  \item Lors de la \kw{rencontre de repêchage intermédiaire} et de la
  \kw{rencontre de repêchage final}, on fait seulement autant de rondes de
  \kw{ballottages réguliers} qu'il est nécessaire.


  \item \label{itm:liste-commissaire} L'ordre de sélection, à l'intérieur de
  chaque ronde de \kw{ballottage}, est déterminé par la \kw{liste du commissaire}.

  \begin{enumerate}

    \item La \kw{liste du commissaire} est établie en fonction de l'ordre
    \emph{inverse} du classement de la saison \emph{précédente} (i.e., le
    \kw{club} ayant terminé dernier obtient le premier choix, le \kw{club} ayant
    terminé avant-dernier obtient le deuxième choix, et ainsi de suite.)

  \end{enumerate}

  \item Un temps maximum de \textbf{cinq minutes} est alloué au \kw{DG} pour
  faire son choix lorsque arrive son rang de sélection.

  \begin{enumerate}

    \item Si, après cinq minutes, le \kw{DG} dont c'est le tour n'a toujours pas
    fait son choix, le \kw{DG} qui possède le choix suivant peut immédiatement
    effectuer sa sélection, puis on revient à nouveau au \kw{DG} ayant préséance
    et ainsi de suite jusqu’à ce qu’il arrête son choix.

  \end{enumerate}

  \item Lors d'une ronde de \kw{ballottage}, un \kw{DG} peut décider de
  conserver son \kw{ballottage} pour cette ronde.

  \begin{enumerate}

    \item Un \kw{ballottage} ainsi conservé ne peut pas être utilisé plus tard
    dans la même \kw{rencontre}, mais il pourra être utilisé lors d'une
    \kw{rencontre} subséquente dans la même saison.

    \item Un \kw{ballottage} inutilisé à la fin d'une saison n’est pas
    transférable à la saison suivante: il est perdu à jamais.

  \end{enumerate}

  \item Un joueur devient \kw{éligible} à partir du moment où il est repêché par
  une équipe de la LNH ou qu'il signe son premier contrat professionnel avec une
  équipe de la LNH.

  \begin{enumerate}

    \item Lorsqu'un joueur devient \kw{éligible}, il le reste pour toujours.

  \end{enumerate}

  \item \label{itm:disponible} Un joueur \kw{disponible} est un joueur
  \kw{éligible} qui n'est présentement la propriété d'aucun \kw{club} de la LQFFCEAN.

  \item Lorsqu'il utilise un \kw{ballottage}, un \kw{DG} peut sélectionner
  n'importe quel joueur \kw{disponible} sans se soucier de la position du
  joueur.
  \begin{enumerate}
    \item À la fin d'une \kw{rencontre}, chaque \kw{club} doit cependant
    respecter le nombre de joueurs prescrit pour chaque position au règlement
    \ref{itm:alignement}, sous peine d'être \kw{suspendu} (voir
    section~\ref{sec:suspension}) jusqu'à ce que la situation soit rectifiée.
  \end{enumerate}

  \item \label{itm:liberation} Un joueur \kw{libéré} est un joueur qui ne
  possède pas de contrat avec une équipe de la LNH lors de la \kw{rencontre de
  repêchage initial}.

  \begin{enumerate}

    \item Un joueur qui possède un contrat avec une équipe de la LNH n'est
    \emph{pas} considéré comme un joueur \kw{libéré}, même s'il est blessé,
    inactif, en grève, suspendu par la LNH, en prison, joue dans une ligue
    autre que la LNH ou quelque autre raison que ce soit.

    \item Le site \url{www.generalfanager.com} sert de référence pour savoir si
    un joueur possède un contrat ou non.

    \item Exception à \ref{itm:liberation}: Si un joueur qui était considéré
    comme un défenseur lors de la saison précédente est maintenant considéré
    comme un joueur d'avant (ou vice versa) sur \url{www.nhl.com} lors de la
    \kw{rencontre de repêchage initial}, le \kw{DG} dont le \kw{club} est
    propriétaire de ce joueur peut demander à ce qu'il devienne un joueur
    \kw{libéré}.

    \item Un joueur dont le contrat est racheté par son équipe de la LNH est
    considéré comme un joueur \kw{libéré}, sauf s'il possède un nouveau contrat
    avec une autre équipe de la LNH.

  \end{enumerate}

  \item Si jamais, suite à la \kw{rencontre de repêchage initial}, un \kw{DG}
  se rend compte que l'un des joueurs de son \kw{club} aurait \emph{du} être
  \kw{libéré}, et que personne n'a remarqué l'erreur, le \kw{DG} sera autorisé
  à sélectionner un joueur \kw{disponible} pour remplacer le joueur qui aurait
  du être \kw{libéré}.
  \begin{enumerate}
    \item Les points du nouveau joueur ne seront comptabilisés
    qu'à partir du moment où le \kw{commissaire} aura été avisé du choix.
  \end{enumerate}

  \item Un \kw{DG} n'est pas obligé de ballotter un joueur \kw{libéré}: il
  peut décider de le conserver.

  \begin{enumerate}

    \item Ce joueur recommence accumuler des points pour son \kw{club} s'il
    redevient actif dans une équipe de la LNH.

    \item Si le nouveau contrat signé par le joueur cause un dépassement du
    \kw{plafond salarial} pour son \kw{club}, le joueur n'accumule aucun point
    pour son \kw{club} tant que celui-ci n'a pas rammené sa \kw{masse salariale}
    sous le \kw{plafond}.

  \end{enumerate}

  \item \label{itm:changements-effectifs} Les changements effectués à
  l'alignement d'un \kw{club} pendant une \kw{rencontre} sont effectifs le
  lendemain de la \kw{rencontre}. C'est-à-dire que les points accumulés par le
  joueur lors des matches ayant lieu le jour même de la rencontre sont
  comptabilisés pour l'ancien \kw{club} du joueur, pas pour son nouveau
  \kw{club}.

  \begin{enumerate}

    \item \label{itm:rencontre-initiale-post-debut-saison} Exception à
    \ref{itm:changements-effectifs}: les changements effectués lors de cette
    \kw{rencontre de repêchage initial} sont effectifs à partir du début de la
    saison de la LNH (de façon rétroactive si nécessaire).

  \end{enumerate}

\end{enumerate}

\section{Plafond salarial}

\begin{enumerate}

  \item La \kw{masse salariale} d'un \kw{club} est la somme du \kw{cap hit}
  de chacun de ses joueurs pour la saison en cours.

  \item Le \kw{cap hit} d'un joueur est le montant total du salaire et des bonus
  prévus pendant la durée de son contrat, divisé par le nombre d'années du
  contrat.

  \begin{enumerate}

    \item Pour le calcul du \kw{cap hit}, les bonus incluent les bonus de
    signature et les bonus de performance, qu'ils soient éventuellement atteints
    ou non.

    \item La référence officielle de la \kw{LQFFCEAN} pour le \kw{cap hit} est
    le site \url{www.generalfanager.com}. Le montant qui correspond au \kw{cap
    hit} de la \kw{LQFFCEAN} est appellé "AAV" (Average Annual Value) sur
    \url{www.generalfanager.com}. C'est le "AAV" qui compte pour la
    \kw{LQFFCEAN}, et non ce que \url{www.generalfanager.com} appelle "cap hit"
    (qui n'inclue pas les bonus).

    \item Lorsque un joueur est échangé dans la LNH et que son ancienne équipe
    retient une partie de son salaire, le \kw{cap hit} du joueur reste
    identique, même si plusieurs équipes de la LNH se partagent celui-ci.

    \item Lorsque le contrat d'un joueur est \emph{racheté} par une équipe de la
    LNH, les pénalités imposées à son ancienne équipe de la LNH ne sont
    \textit{pas} comptabilisées par la \kw{LQFFCEAN}. Si le joueur a un nouveau
    contrat, seul le \kw{cap hit} du nouveau contrat est pris en compte.

  \end{enumerate}

  \item \label{itm:plafond} Le \kw{plafond salarial} pour chaque \kw{club} est
  égal à celui de la LNH pour la saison en cours.

  \item Chaque \kw{DG} est responsable de s'assurer que la \kw{masse salariale}
  de son \kw{club} soit inférieure ou égale au \kw{plafond salarial} à la fin de
  chaque \kw{rencontre}.

  \item \label{itm:depassement-plafond} En cas de dépassement du \kw{plafond}
  suite à une \kw{rencontre}, le \kw{club} fautif est \kw{suspendu} (voir
  section~\ref{sec:suspension}) à partir du moment où l'erreur est connue du
  \kw{commissaire}.

  \begin{enumerate}

    \item La \kw{suspension} peut être levée à partir du moment où le \kw{DG} du
    \kw{club suspendu} ramène sa \kw{masse salariale} sous le \kw{plafond} par
    l'intermédiaire d'un \kw{échange}.

    \item Si, après \textbf{10 jours} à partir du moment où le dépassement est
    connu du \kw{commissaire}, le \kw{club suspendu} n'a pas corrigé sa
    situation, il est \emph{immédiatement} \kw{dissous} (\ref{itm:dissolution})
    et son \kw{propriétaire} est exclu de la LQFFCEAN pour au moins une saison
    (\ref{itm:exclusion-apres-dissolution}).

  \end{enumerate}

\end{enumerate}

\section{Échanges}

\begin{enumerate}

  \item Il n'y a pas de limite au nombre d'\kw{échanges} pouvant être faits par
  un \kw{club} au cours d'une saison.

  \item Les \kw{échanges} peuvent être réalisés en tout temps, \emph{sauf} entre
  la date limite des échanges de la LNH et la fin de la saison.

  \begin{enumerate}

    \item Lorsqu'un \kw{échange} est effectué \emph{pendant} une \kw{rencontre},
    il est effectif le lendemain de la \kw{rencontre} (exception:
    \ref{itm:rencontre-initiale-post-debut-saison}).

    \item Lorsqu'un \kw{échange} est effectué \emph{à l'extérieur} d'une
    \kw{rencontre}, il est effectif le lendemain de la date où le
    \kw{commissaire} est avisé de l'échange.

  \end{enumerate}

  \item Seuls des joueurs et des \kw{ballottages} peuvent faire partie d'un
  échange. Toute forme de compensation extérieure au pool (\textit{e.g.}, de
  l'argent) est strictement interdite.

  \item \label{itm:nb-joueurs-echange} Lors d'un échange, le nombre de joueurs
  donné par un \kw{club} doit être égal au nombre de joueurs reçus (exception:
  \ref{itm:echange-ballotage-expansion}).

  \item Tous les types de \kw{ballottage} peuvent faire partie d'un échange.

  \begin{enumerate}

    \item Lorsqu'un \kw{ballottage régulier} est échangé, il est nécessaire de
    spécifier la saison, la \kw{ronde}, et le \kw{club} à qui appartenait
    initialement le \kw{ballottage} (puisqu'un \kw{ballottage} acquis d'un autre
    club peut être ré-échangé par la suite).

    \item Échanger un \kw{ballottage de libération} équivaut à échanger le
    joueur \kw{libéré}.

    \item \label{itm:echange-ballotage-expansion} Pour les besoins du
    règlement~\ref{itm:nb-joueurs-echange}, un \kw{ballottage d'expansion}
    compte comme un joueur (puisqu'il représente, à toutes fins pratiques, une
    place dans l'alignement du \kw{club}).

  \end{enumerate}

  \item Un \kw{échange} effectué \emph{pendant} une rencontre peut avoir pour
  effet qu'un des \kw{clubs} impliqués dans l'\kw{échange} ne respecte pas,
  momentanément, le \kw{plafond salarial} (\ref{itm:plafond}) ou le nombre de
  joueurs par position (\ref{itm:alignement}).

  \begin{enumerate}

    \item Chaque club doit néanmoins s'assurer de respecter le \kw{plafond
    salarial} (voir~\ref{itm:depassement-plafond}) et le nombre de joueurs par
    position à la fin de la \kw{rencontre}, sous peine de \kw{suspension} (voir
    section~\ref{sec:suspension}).

  \end{enumerate}

  \item Si un \kw{échange} effectué \emph{à l'extérieur} d'une \kw{rencontre} a pour
  effet qu'un des \kw{clubs} impliqués dans l'\kw{échange} ne respecte pas le
  \kw{plafond salarial} (\ref{itm:plafond}) ou le nombre de joueurs par position
  (\ref{itm:alignement}), le \kw{commissaire} annulera l'échange.

\end{enumerate}

\section{Alignement}
\begin{enumerate}
  \item \label{itm:alignement} Chaque \kw{club} est composé de 15 joueurs:
  \begin{itemize}
    \item 9 attaquants
    \item 4 défenseurs
    \item 2 gardiens
  \end{itemize}
\end{enumerate}

\section{Cumul des points}
\begin{enumerate}

  \item Attaquants, défenseurs et gardiens:
  \begin{itemize}
    \item un but = 1 point
    \item une passe = 1 point
  \end{itemize}

  \item Gardiens:
  \begin{itemize}
    \item Une victoire = 2 points
    \item Une victoire par blanchissage = 3 points
    \item Une défaite en prolongation = 1 point
    \item Une défaite en tirs de barrage = 1 point
  \end{itemize}

  \item En cas d'égalité des points au classement à la fin de la saison, le
  \kw{club} qui aura obtenu la plus haute moyenne de points par matches joués
  sera privilégié. Si l'égalité subsiste, le nombre total de buts sera utilisé
  comme deuxième critère.

\end{enumerate}

\section{Coût de participation et prix}

\begin{enumerate}

  \item Le \kw{coût de participation} de la LQFFCEAN est de 50\$ par saison.

  \item Le \kw{coût de participation} doit être payé au \kw{commissaire} lors de
  la \kw{rencontre de repêchage initial}.

  \item Au terme de la saison, le montant total des \kw{coûts de participation}
  est remis en \kw{prix} aux \kw{propriétaires} des \kw{clubs} ayant terminé
  aux trois premières positions, selon les proportions suivantes:
  \begin{itemize}
    \item Premier: 60\%
    \item Deuxième: 30\%
    \item Troisième: 10\%
  \end{itemize}

\end{enumerate}

\section{Dissolution}

\begin{enumerate}

  \item Lorsqu'un \kw{propriétaire} quitte la \kw{LQFFCEAN}, son \kw{club} est
  \kw{dissous}.

  \item \label{itm:dissolution} Au moment de la \kw{dissolution} d'un \kw{club},
  les joueurs de ce \kw{club} redeviennent \kw{disponibles}.

  \item Si un \kw{propriétaire} désire quitter la \kw{LQFFCEAN}, il doit en
  aviser le \kw{commissaire} \emph{avant} la \kw{rencontre de repêchage
  initial}, sinon son \kw{club} ne sera \kw{dissous} qu'à la fin de la saison.

  \begin{enumerate}

    \item Le \kw{coût de participation} n'est \emph{pas} remboursable.

    \item \label{itm:droits-avant-dissolution} Un \kw{propriétaire} qui désire
    quitter à la fin de la saison n'est pas obligé de prendre part aux activités
    de la \kw{LQFFCEAN}, mais il conserve tous ses droits, y compris celui de
    récolter son prix si son \kw{club} termine dans les trois premiers.

    \item Exception à \ref{itm:droits-avant-dissolution}: si un
    \kw{propriétaire} ayant annoncé son intention de quitter essaie de conclure
    un \kw{échange} déraisonnable, le \kw{commissaire} peut refuser
    l'\kw{échange}.

  \end{enumerate}

  \item Dans le cas où un \kw{club dissous} avait échangé des \kw{ballottages}
  futurs, ceux-ci sont ajoutés à la fin de la ronde de \kw{ballottage} où ils
  étaient prévus, pour que le \kw{club} qui est maintenant en leur possession
  puisse les utiliser.

  \begin{enumerate}

    \item Dans le cas où plusieurs \kw{ballottages} de \kw{clubs dissous}
    doivent être utilisés dans une même ronde, l'ordre de ceux-ci est déterminé
    au hasard.

  \end{enumerate}

  \item \label{itm:exclusion-apres-dissolution} Le \kw{propriétaire} d'un
  \kw{club dissous} est exclu de la \kw{LQFFCEAN} pendant au moins une saison.

  \begin{enumerate}

    \item S'il souhaite ensuite réintégrer la \kw{LQFFCEAN}, il doit se soumettre
    au processus d'\kw{expansion} comme n'importe quel autre \kw{candidat}.

  \end{enumerate}

  \item La \kw{dissolution} est la seule issue possible lorsqu'un
  \kw{propriétaire} quitte la \kw{LQFFCEAN}. Son \kw{club} ne peut pas être
  transféré à une nouvelle personne.

\end{enumerate}

\section{Expansion}
\label{sec:expansion}

\begin{enumerate}

  \item Les \kw{candidats} souhaitant obtenir un \kw{club d'expansion} sont
  invités à assister à la \kw{rencontre de pré-saison} pour mousser leur
  candidature.

  \item La décision d'accorder ou non un \kw{club d'expansion} est votée pour
  chaque \kw{candidat}.
  \begin{enumerate}
    \item Ces votes sont tenus lors d'une "pause détente" dont sont exclus les \kw{candidats}.
  \end{enumerate}

  \item Lorsqu'une \kw{expansion} a lieu, les \kw{DG} des \kw{clubs} existants
  doivent soumettre au \kw{commissaire} une liste d'au moins \textbf{un}
  \kw{joueur non-protégé} par équipe d'expansion avant la \kw{rencontre de
  repêchage initial}.

  \item Les \kw{DG} des \kw{clubs d'expansion} peuvent sélectionner autant de
  joueurs qu'ils le désirent dans la banque de \kw{joueurs non-protégés}.

  \begin{enumerate}

    \item Cette sélection se fait lors de la \kw{rencontre de repêchage
    initial}, \emph{avant} la première ronde de \kw{ballottages réguliers}.

    \item \label{itm:ordre-expansion} S'il y a plusieurs \kw{clubs d'expansion},
    l'ordre de sélection entre eux se joue à roche-papier-ciseaux (une série
    "deux de trois").

    \item La sélection se fait en alternance entre chaque \kw{club d'expansion}
    selon l'ordre déterminé en \ref{itm:ordre-expansion}.

  \end{enumerate}

  \item Les \kw{clubs d'expansion} reçoivent le même nombre de \kw{ballottages
  réguliers} que les \kw{clubs} existants (voir \ref{itm:nombre-ballottages}).

  \begin{enumerate}

    \item Les \kw{clubs d'expansion} s'ajoutent à la fin de la \kw{liste du
    commissaire} (voir \ref{itm:liste-commissaire}), à la suite du \kw{club}
    ayant terminé premier lors de la saison précédente, dans l'ordre déterminé à
    \ref{itm:ordre-expansion}.

    \item Les \kw{clubs d'expansion} doivent utiliser \emph{tous} leurs
    \kw{ballottages réguliers} lors de la \kw{rencontre de repêchage initial}.

  \end{enumerate}

  \item Après toutes les rondes de ballottage (\ref{itm:rondes-ballottages}),
  les clubs d'expansion dont l'alignement n'est pas encore complet
  (\ref{itm:alignement}) peuvent sélectionner les joueurs qui leur manquent dans
  la banque de \kw{joueurs disponibles} en alternance selon l'ordre déterminé en
  \ref{itm:ordre-expansion}.

  \item Le \kw{DG} d'un \kw{club d'expansion} n'acquiert le droit de vote qu'au
  moment où son \kw{club} atteint le nombre de joueurs prévu au règlement
  \ref{itm:alignement}.

\end{enumerate}

\section{Suspension}
\label{sec:suspension}

\begin{enumerate}

  \item Un \kw{club} qui est \kw{suspendu} n'accumule aucun point pendant la
  durée de sa \kw{suspension}.

  \item Le \kw{club suspendu} recommence à accumuler des points dès que la
  \kw{suspension} est levée par le \kw{commissaire}.

  \item Le \kw{DG} d'un \kw{club suspendu} conserve son droit de vote pendant
  la durée de la suspension.

\end{enumerate}

\end{multicols}
\end{document}
